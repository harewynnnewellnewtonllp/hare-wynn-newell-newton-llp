Hare, Wynn, Newell & Newton, LLP



Hare Wynn is a nationally-acclaimed personal injury law firm that has stood up for the rights of injured people across the United States for 125 years. Our firm has recovered billions of dollars of compensation on behalf of our clients, and we’re ready to fight for you.



Address: 325 W Main St, #210, Lexington, KY 40507, USA


Phone: 859-550-2900


Website: https://www.hwnninjurylaw.com/lexington
